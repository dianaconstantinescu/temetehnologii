const express = require('express');
const bodyParser = require('body-parser');
const lodash=require('lodash');
const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});
app.put('/modify/:id',(req,res)=>{
    const id = req.params.id;
  
    if(id<products.length){
      if(req.body.productName){
        products[id].productName=req.body.productName;
      }
      if(req.body.price){
        products[id].price=req.body.price;
      }
      
      res.status(200).send('ok');
    }else{
      res.status(500).send('Error!');
    }
});
app.delete('/deleteProduct',(req,res)=>{
 const denumire=req.body.productName;
 let id=-1;
  products.forEach((product)=>{
    if(product.productName==denumire){
      id=product.id;
    }
    
  });
  if(id!=-1){
      products.splice(id,1);
      res.status(200).send(`Deleted the product with id ${id}`);
      
    }else{
      res.status(500).send(`Could not find an item with this name: ${denumire}`);
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});