import React, { Component } from 'react';
import './App.css';

import {AddProduct} from './components/addproduct/AddProduct';
import {ProductList} from './components/productlist/ProductList';

class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.products = [];
  }
  
 
  onProductAdded=(product)=>{
    let newproducts=this.state.products;
    newproducts.push(product);
    this.setState({products:newproducts});
  }
  componentDidMount(){ 
    fetch('https://webtechnologies-dianaconstantinescu.c9users.io:8081/get-all').then((res)=>res.json()).then((newproducts)=>this.setState({products:newproducts}));
  }
  render() {
    return (
      <React.Fragment>
        <AddProduct productAdded={this.onProductAdded}/>
        <ProductList title="Products" source={this.state.products} />
      </React.Fragment>
    );
  }
}

export default App;


  