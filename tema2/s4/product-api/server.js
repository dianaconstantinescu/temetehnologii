const express = require('express');
const bodyParser = require('body-parser');
var cors=require('cors');
const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    
    res.setHeader('Access-Control-Allow-Methods', 'Content-Type, X-Requested-Width, x-request-metadata');
    res.status(200).send(products);
})
const corsHeaders={
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
    "Access-Control-Allow-Methods": "Content-Type, X-Requested-Width, x-request-metadata"
}
app.options("/add",cors())

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.listen(8081, () => {
    console.log('Server started on port 8081...');
});